namespace WcfServiceNorthWind
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Runtime.Serialization;

    [DataContract]
    public partial class Supplier
    {

        [DataMember]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long SupplierID { get; set; }

        [DataMember]
        [Required]
        [StringLength(2147483647)]
        public string CompanyName { get; set; }

        [DataMember]
        [StringLength(2147483647)]
        public string ContactName { get; set; }

        [DataMember]
        [StringLength(2147483647)]
        public string ContactTitle { get; set; }

        [DataMember]
        [StringLength(2147483647)]
        public string Address { get; set; }

        [DataMember]
        [StringLength(2147483647)]
        public string City { get; set; }

        [DataMember]
        [StringLength(2147483647)]
        public string Region { get; set; }

        [DataMember]
        [StringLength(2147483647)]
        public string PostalCode { get; set; }

        [DataMember]
        [StringLength(2147483647)]
        public string Country { get; set; }

        [DataMember]
        [StringLength(2147483647)]
        public string Phone { get; set; }

        [DataMember]
        [StringLength(2147483647)]
        public string Fax { get; set; }

        [DataMember]
        [StringLength(2147483647)]
        public string HomePage { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfServiceNorthWind
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class NorthWindService : INorthWindService
    {
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

        public List<Supplier> GetSuppliers()
        {
            using (var context = new ADO_Model())
            {
                var suppliers = context.Suppliers;

                var x = from s in suppliers select s;
                return x.ToList();
            }

            //var list = new List<Supplier> { new Supplier { CompanyName = "test" } };
            //return list;
        }

        public List<Supplier> GetSupplierListFromPartialName(string name)
        {
            using (var context = new ADO_Model())
            {
                var myList = new List<Supplier>();
                if (name == null) return myList;

                var suppliers = context.Suppliers;
                var sups = (from sup in suppliers
                            where sup.CompanyName.Contains(name)
                            select sup);
                if (sups != null)
                {
                    foreach (Supplier sup in sups)
                    {
                        var mySup = new Supplier();
                        mySup.SupplierID = sup.SupplierID;
                        mySup.CompanyName = sup.CompanyName;
                        mySup.ContactName = sup.ContactName;
                        mySup.ContactTitle = sup.ContactTitle;
                        mySup.Address = sup.Address;
                        mySup.City = sup.City;
                        mySup.Region = sup.Region;
                        mySup.PostalCode = sup.PostalCode;
                        mySup.Country = sup.Country;
                        mySup.Phone = sup.Phone;
                        mySup.Fax = sup.Fax;
                        mySup.HomePage = sup.HomePage;
                        myList.Add(mySup);
                    }
                }
                return myList;
            }
            
        }

        public bool AddSupplier(string companyName, string contactName, string contactTitle, string address, string city, string region, string postalCode, string country, string phone, string fax, string homePage)
        {
            var mySup = new Supplier()
            {
                //SupplierID = id,
                CompanyName = companyName,
                ContactName = contactName,
                ContactTitle = contactTitle,
                Address = address,
                City = city,
                Region = region,
                PostalCode = postalCode,
                Country = country,
                Phone = phone,
                Fax = fax,
                HomePage = homePage
            };

            using (var context = new ADO_Model())
            {
                var suppliers = context.Suppliers;
                
                suppliers.Add(mySup);

                try
                {
                    context.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            return true;

        }

        public bool UpdateSupplier(long id, string companyName, string contactName, string contactTitle, string address, string city, string region, string postalCode, string country, string phone, string fax, string homePage)
        {
            using (var context = new ADO_Model())
            {
                var suppliers = context.Suppliers;
                var sup = (from s in suppliers
                             where s.SupplierID == id
                             select s).SingleOrDefault();
                if (sup == null) return false;

                sup.SupplierID = id;
                sup.CompanyName = companyName;
                sup.ContactName = contactName;
                sup.ContactTitle = contactTitle;
                sup.Address = address;
                sup.City = city;
                sup.Region = region;
                sup.PostalCode = postalCode;
                sup.Country = country;
                sup.Phone = phone;
                sup.Fax = fax;
                sup.HomePage = homePage;

                try
                {
                    context.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                return true;
            }
        }

        public bool DeleteSupplier(long id)
        {
            using (var context = new ADO_Model())
            {
                var suppliers = context.Suppliers;
                var sup = (from s in suppliers
                           where s.SupplierID == id
                           select s).SingleOrDefault();
                if (sup == null) return false;

                suppliers.Remove(sup);

                try
                {
                    context.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                return true;
            }

        }
    }
}

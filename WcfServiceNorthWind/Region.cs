namespace WcfServiceNorthWind
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Region
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long RegionID { get; set; }

        [Required]
        [StringLength(2147483647)]
        public string RegionDescription { get; set; }
    }
}

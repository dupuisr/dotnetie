﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplicationContoso.Models
{
    public class CategorieModel
    {
        public int Id { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }

    }

}
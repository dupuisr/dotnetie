﻿namespace WebApplicationContoso.Models
{
    public class ProductModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Fournisseur { get; set; }
        public string Categorie { get; set; }
        public string EnStock { get; set; }
        public string Abandon { get; set; }
        public string Price { get; set; }
        public string QuantityPerUnit { get; set; }
        public string QuantityPerOrder { get; set; }
        public string ReorderLevel { get; set; }

    }
}
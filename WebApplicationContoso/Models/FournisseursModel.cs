﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplicationContoso.Models
{
    public class FournisseursModel
    {
        public long Id { get; set; }
        public String Name { get; set; }//CompanyName
        public String ContactName { get; set; }
        public String ContactTitle { get; set; }
        public String Address { get; set; }
        public String Ville { get; set; }
        public String PostalCode { get; set; }
        public String Region { get; set; }
        public String Pays { get; set; }
        public String Tel { get; set; }
        public String Fax { get; set; }
        public String WebSite{ get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplicationContoso.Models;

namespace WebApplicationContoso.Controllers
{
    public class FournisseursController : Controller
    {
        


        public ActionResult GetDetails(long Id) {
            FournisseursModel c = new FournisseursModel();
            return View(c);
        }
        // GET: Fournisseurs
        public ActionResult Add()
        {
            FournisseursModel c = new FournisseursModel();
            return View(c);//(Content("HelloFournisseurs"));
        }

        [HttpPost]
        public ActionResult Add(FournisseursModel c)
        {
            //Ajout du modèle à la bd
            ServiceReferenceNW.NorthWindServiceClient client = new ServiceReferenceNW.NorthWindServiceClient();
            //client.AddSupplier(c.Id, c.Name, c.ContactName, c.ContactTitle, c.Address, c.Ville, c.Region, c.PostalCode, c.Pays, c.Tel, c.Fax, c.WebSite));
            return View();//(Content("HelloFournisseurs"));
        }

        public ActionResult Delete()
        {
            return View();//(Content("HelloFournisseurs"));
        }

        [HttpPost]
        public ActionResult Delete(long id)
        {
            ServiceReferenceNW.NorthWindServiceClient client = new ServiceReferenceNW.NorthWindServiceClient();
            //client.Delete(id);
            Console.WriteLine(id);
            //client.DeleteSupplier(id);
            //FournisseursModel c = new FournisseursModel();
            return RedirectToAction("List");//(Content("HelloFournisseurs"));
        }

        public ActionResult Search()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Search(String Nom)
        {
            ServiceReferenceNW.NorthWindServiceClient client = new ServiceReferenceNW.NorthWindServiceClient();
            client.GetSupplierListFromPartialName(Nom);
            return View();
        }

        public ActionResult Modify() {
            return View();
        }

        [HttpPost]
        public ActionResult Modify(FournisseursModel c)
        {
            ServiceReferenceNW.NorthWindServiceClient client = new ServiceReferenceNW.NorthWindServiceClient();
            //client.UpdateSupplier(c.Id, c.Name, c.ContactName, c.ContactTitle, c.Address, c.Ville, c.Region, c.PostalCode, c.Pays, c.Tel, c.Fax, c.WebSite);
            return View(c);
        }

        public ActionResult List() {
            List<FournisseursModel> c = new List<FournisseursModel>();
            ServiceReferenceNW.NorthWindServiceClient m = new ServiceReferenceNW.NorthWindServiceClient();
            var tab = m.GetSuppliers();
            foreach(var i in tab){
                FournisseursModel f = new FournisseursModel {
                    Id=i.SupplierID,
                    Name=i.CompanyName,
                    ContactName= i.ContactName,
                    ContactTitle=i.ContactTitle,
                    Address = i.Address,
                    Ville = i.City,
                    PostalCode = i.PostalCode,
                    Region = i.Region,
                    Pays = i.Country,
                    Tel = i.Phone,
                    Fax = i.Fax,
                    WebSite = i.HomePage
                };
            c.Add(f);
            }
            return View(c);
        }
    }
}
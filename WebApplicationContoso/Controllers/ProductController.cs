﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplicationContoso.Models;

namespace WebApplicationContoso.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult GetDetails(String Name) {
            ProductModel c = new ProductModel();
            return View(c);
        }
        
        public ActionResult Add()
        {
            ProductModel c = new ProductModel();
            return View(c);//(Content("HelloFournisseurs"));
        }

        [HttpPost]
        public ActionResult Add(ProductModel c)
        {
            //Ajout du modèle à la bd
            ServiceReferenceNW.NorthWindServiceClient client = new ServiceReferenceNW.NorthWindServiceClient();

            return RedirectToAction("Add");//(Content("HelloFournisseurs"));
        }

        public ActionResult Delete()
        {
            ProductModel c = new ProductModel();
            return View(c);//(Content("HelloFournisseurs"));
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            ServiceReferenceNW.NorthWindServiceClient client = new ServiceReferenceNW.NorthWindServiceClient();
            //client.Delete(id);
            ProductModel c = new ProductModel();
            return View();//(Content("HelloFournisseurs"));
        }

        public ActionResult Search()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Search(String Nom)
        {
            ServiceReferenceNW.NorthWindServiceClient client = new ServiceReferenceNW.NorthWindServiceClient();
            //ProductMode m = client.Search(Nom)
            //return RedirectToAction(GetDetails(m));
            return RedirectToAction("GetDetails");
        }

        public ActionResult Modify() {

            //DB.get(m);
            ProductModel c = new ProductModel();
            return View(c);
        }

        [HttpPost]
        public ActionResult Modify(ProductModel c)
        {
            ServiceReferenceNW.NorthWindServiceClient client = new ServiceReferenceNW.NorthWindServiceClient();
            //client.Update(c);
            return View(c);
        }

        public ActionResult List() {
            IEnumerable<ProductModel> c = new List<ProductModel>();
            return View(c);
        }
    }
}